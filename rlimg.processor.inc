<?php
// $Id$

/**
 * RandomCore Filter Processors
 * 
 * This file defines the parsing utility classes 'RlimgProcessor' 
 * and 'RlimgBlockProcessor'. These classes wrap all functionality
 * needed for filtering text for the 'rlimg' and 'rlimg_block' 
 * filters defined in rlcore.filter.inc.
 * 
 * @author    zourtney <zourtney@randomland.net>
 * @copyright randomland.net
 * @since     2010, last updated 2011-01-28
 * @package   RandomFilter
 * @version   1.0 beta
 */


/**
 * Base image processor class.
 * 
 * Processes [rlimg] tags and an arbitrary set of XML-style 
 * attributes.
 * 
 * Image tags may be entered in any of the following forms:
 * - [rlimg $id]
 * - [rlimg $id $att="$attValue" ...]
 * - [rlimg id="$id"]
 * - [rlimg id="$id" $att="$attValue" ...]
 * 
 */
class RlimgProcessor {
  /***************************************************************
   * Static Attributes
   ***************************************************************/
  /**
   * Filename of the template used to theme 'rlimg' nodes.
   * 
   * Defined as 'rlcore.filter.image'.
   * @var string TEMPLATE
   */
  const TEMPLATE = 'rlcore.filter.image';
  
  //const CSS = 'rlimg_image.css';
  
  /**
   * Array of allowed attributes.
   * 
   * Ideally this would be static and const. But this does not 
   * seem to be possible with PHP.
   * 
   * See {@link processAttribute processAttribute()} for a full
   * description on attributes.
   * 
   * @staticvar array
   */
  public static $VARIABLES = array( 
    'align' => NULL,
    'alt' => NULL,
    'caption' => NULL,
    'class' => NULL,
    'description' => NULL,
    'imagepath' => NULL,
    'imageuri' => NULL,
    'imageurl' => NULL,
    'inblock' => NULL,
    'fid' => NULL,
    'url' => NULL,
    'preset' => NULL,
    'style' => NULL,
  );
  
	
  /***************************************************************
   * Member Functions
   ***************************************************************/
  /**
   * Constructor. Takes original text as sole parameter.
   * @param string $text
   */
  public function __construct($text) {
    // Set class attributes
    $this->attRegex = '/([a-zA-Z_][a-zA-Z0-9_]*+)="([^"]*([^"]*)*)"/';
    //$this->css = self::CSS;
    $this->inBlock = FALSE;
    $this->regex = '%\[rlimg\s+([0-9]+|id\s*=\s*"?([0-9]+)"?)\s*([^\]]*)\s*\]%';
    $this->template = self::TEMPLATE;
    $this->text = $text;
    $this->variables = self::$VARIABLES;
  }
  
  /**
   * Performs the filtering action.
   * 
   * @return string The filtered text of the entire input string
   */
  public function process() {
    $callback = array($this, 'replaceCallback');
    return preg_replace_callback($this->regex, $callback, $this->text);
  }
  
  /**
   * Performs transformation processing for a single match.
   * 
   * This callback function is called for each match found from
   * the preg_replace_callback() function in {@link process() process()}.
   * 
   * The bulk of the transformation work is done here, in the
   * following order:
   * 1. Parse attributes
   * 2. Create an attribute called 'image' (which is passed to the 
   *    template) which is the HTML as themed by the ImageCache call
   *    theme('image_style', ...);
   * 3. Create HTML as themed by template {@link TEMPLATE TEMPLATE}
   * 
   * @param array $matches
   * 
   * @return string The filtered text for a single match
   */
  protected function replaceCallback($matches) {
    // Parse the attributes into an array
    $params = $this->parseParams($matches);
    
    // Pass them the formatted image as well
    $params['image'] = theme('image_style', array(
      'style_name' => $params['preset'],
      'path' => $params['imageuri'],
      'alt' => $params['alt'],
      /*'title' => $params['caption']*/)
    );

    //return theme($this->template, $params);
    
    // Return the themed HTML
    return '[[{' .
      '"type":"media",' .
      '"view_mode":"media_preview",' .
      '"fid":"' . $params['fid'] . '",' .
      '"attributes":{' .
      	'"alt":"' . $params['alt'] . '",' .
        '"class":"' . $params['class'] . '",' .
        '"format":"media_preview",' .
        '"title":"",' .
        '"typeof":"foaf:Image"' .
      '}}]]'
    ;
  }
  
  /**
   * Parses attribute string in an array keyed by attribute name.
   * 
   * If the 'id' attribute was defined in shorthand (ie [rlimg 5])
   * this function will parse it out and create an entry in the
   * array, just as any other attribute.
   * 
   * @param array $matches Array of groups for a single match
   * as obtained by the preg_replace_callback call in {@link process() process()}
   * and processed by the {@link RlimgProcessor::replaceCallback() replaceCallback()}.
   * 
   * @return array Array of attributes, keyed by attribute name.
   */
  protected function parseParams($matches) {
    // If we're using id="xxx" syntax, the ID is stored in
    // the 3rd position. Otherwise it will be in the 2nd.
    if (empty($matches[2])) {
      $fileID = $matches[1];
    }
    else {
      $fileID = $matches[2];
    }
    
    // Load the file node
    $file = file_load($fileID);

    // Make sure the node ID gets parsed
    $attString = 'fid="' . $fileID . '" ' . $matches[3];
		
    // Run expression, looking for pair like: attribute="value"
    preg_match_all($this->attRegex, $attString, $matches, PREG_SET_ORDER);
    
    // Put attributes into associative array
		foreach ($matches as $val) {
      $name = strtolower($val[1]);
      
      // Only pass along varibles defined in the class spec
      if (array_key_exists($name, $this->variables)) {
        $atts[$name] = $val[2];
      }
    }
    
    // Get defaults (or other post-processing)
    foreach ($this->variables as $name => $value)	{
      $val = isset($atts[$name]) ? $atts[$name] : NULL;
      $atts[$name] = $this->processAttribute($file, $name, $val);
    }
    
    //BUG: this try...catch doesn't work because the current
    //     PHP warning level is set too low (or something).
    /*try
    {
      $styles = image_styles( );
      $sizes = $styles[$atts['preset']]['effects'][0]['data'];
    }
    catch ( Exception $e )
    {
      // log error?
      $sizes = array( 'width' => 0, 'height' => 0 );
    }
    
    $atts['imagewidth'] = $sizes['width'];
    $atts['imageheight'] = $sizes['height'];
    */
    return $atts;
  }
  
  /**
   * Processes a single attribute and returns a cleaned value.
   * 
   * This function will accept a single attribute name and value.
   * If the value is unset or invalid, a default value will be
   * provided.
   * 
   * The available attributes are:
   * - align: the alignment value of the entire image and 
   *   surrounding styling. The available values are 'left',
   *   'center', 'right', and 'none'.
   * - alt: the ALT text to associate with the image object. This
   *   value is read by screen readers.
   * - caption: the short description of the image. It is best to
   *   limit this to about 30-40 characters, depending of how you
   *   style your output.
   * - class: a CSS class name whose styling will get added to
   *   the top-level node. Use this to override styling on a single
   *   call.
   * - description: the long description of the image.
   * - inblock: whether or not this image in containing within
   *   [rlimg-block]...[/rlimg-block] tags.
   * - imagepath: relative system path to the image on disk.
   * - imageuri: relative URL to the image (@todo: check this).
   * - imageurl: full URL to the image (@todo: check this).
   * - nodeurl: URL to the image node
   * - preset: ImageCache preset name as a string. In general,
   *   this will control the size of the image displayed. Drupal 7
   *   defaults are "thumbnail", "medium", and "large". If no
   *   preset is specified, "thumbnail" is used. 
   * - style: inline CSS styling applied to the top-level of
   *   the themed output. Use this to override styling on a
   *   single call. If you plan to reuse styling, consider
   *   using the 'class' attribute of creating a custom template
   *   override.
   * 
   * @param object $file Image file to load
   * @param string $name The attribute name
   * @param string $value The value of the attribute (if any).
   * 
   * @return string Cleaned attribute value.
   */
  protected function processAttribute($file, $name, $value) {
    // This function may get called before the node is set.
    // If so, just exit out.
    if (! $file) {
      return NULL;
    }
    
    return $value;
    
    // Define variables if needed
    /*switch ($name) {
      case 'alt':
      case 'imagepath':
      case 'imageuri':
      case 'imageurl':
        $image = $file->rlfield_image['und'][0];
        break;
      case 'caption':
      case 'description':
        $desc = $file->body;
        break;
    }*/
    
    // Return formatted value
    /*switch ($name) {
      case 'align':
        if ($value == 'left')
          return 'rlimg-align-left';
        if ($value == 'center')
          return 'rlimg-align-center';
        if ($value == 'right')
          return 'rlimg-align-right';
        return 'rlimg-align-none';
      case 'alt':
        if (isset($value))
          return $value;
        
        // Try to get the alt data
        $alt = $image['alt'];
        if (! empty($alt))
          return $alt;
        return 'Image ' . $file->nid;
      case 'caption':
        if (isset($value))
          return $value;
        
        // Try to get the caption text
        if (isset($desc) && isset($desc['summary']))
          return $desc['summary'];
        return 'Image ' . $file->nid;
      case 'class':
        if (isset($value))
          return $value;
        return 'rlimg-image';
      case 'description':
        if (isset($value))
          return $value;
        
        // Try to get the body text
        if (isset($desc) && isset($desc['value']))
          return $text;
        return 'Image ' . $file->nid;
      case 'inblock':
        if (isset($value))
          return $value;
        return $this->inBlock;
      case 'imagepath':
        if (isset($value))
          return $value;
        return drupal_realpath($image['uri']);
      case 'imageuri':
        if (isset($value))
          return $value;
        return $image['uri'];
      case 'imageurl':
				if (isset($value))
          return $value;
        return file_create_url($image['uri']);
      case 'nodeurl':
        if (isset($value))
          return $value;
        return url('node/' . $file->nid, array('absolute' => TRUE));
      case 'preset':
        if (isset($value))
          return $value;
        return 'thumbnail';
      case 'style':
        if (isset($value))
          return $value;
        return '';
    }
    
    return '';*/
  }
}


/**
 * Image block text processor class. Processes [rlimg-block] tags 
 * and an arbitrary set of XML-style attributes. This class 
 * extends the RlimgProcessor class because core functionality
 * is very similar. 
 * 
 * Image block tags may be entered in either of the following 
 * forms:
 * - [rlimg-block]...[/rlimg-block]
 * - [rlimg-block $att="$attValue"]...[/rlimg-block]
 * 
 */
class RlimgBlockProcessor extends RlimgProcessor {
  /***************************************************************
   * Static Attributes
   ***************************************************************/
  /**
   * Filename of the template used to theme 'rlimg' nodes.
   * 
   * Defined as 'rlcore.filter.block'.
   * @var string TEMPLATE
   */
  const TEMPLATE = 'rlcore.filter.block';
  
  //const CSS = 'rlimg_block.css';
  /**
   * Array of allowed attributes.
   * 
   * Ideally this would be static and const. But this does not 
   * seem to be possible with PHP.
   * 
   * See {@link RlimgBlockProcessor::processAttribute processAttribute()}
   * for a full description on attributes.
   * 
   * @staticvar array
   */
  public static $VARIABLES = array( 
    'align' => NULL,
    'class' => NULL,
    'style' => NULL,
  );
  
  /***************************************************************
   * Member Functions
   ***************************************************************/
  /**
   * Constructor. Takes original text as sole parameter.
   * @param string $text
   */
  public function __construct($text) {
    // Call parent constructor
    parent::__construct($text);
    
    // Override these attributes
    //$this->css = self::CSS;
    $this->regex = '/\[rlimg-block\s*([^\]]*)\s*[^\]]*\](.*?)\[\/rlimg-block\]/s';
    $this->template = self::TEMPLATE;
    $this->variables = self::$VARIABLES;
  }
  
  /**
   * Performs transformation processing for a single match.
   * 
   * This callback function is called for each match found from
   * the preg_replace_callback() function in 
   * {@link RlimgBlockProcessor::process() process()}.
   * 
   * The bulk of the transformation work is done here, in the
   * following order:
   * 1. Parse attributes
   * 2. Create an attribute called 'blockcontents' (which is 
   *    passed to the template) which is the HTML between the  
   *    [rlimg-block] and [/rlimg-block] tags.
   * 3. Create HTML as themed by template 
   *    {@link RlimgBlockProcessor::TEMPLATE TEMPLATE}
   * 
   * @param array $matches
   * 
   * @return string The filtered text for a single match
   */
  protected function replaceCallback($matches) {
    // Parse parameters out of starting [rlimg-block] tag
    $params = $this->parseParams($matches);
    
    // Pass entire block contents to the template as well,
    // just in case they want to do something with it
    $params['blockcontents'] = $matches[2];
    
    // Return the themed HTML
    return theme($this->template, $params);
  }
  
   /**
   * Parses attribute string in an array keyed by attribute name.
   * 
   * @param array $matches Array of groups for a single match
   * as obtained by the preg_replace_callback call in 
   * {@link RlimgBlockProcessor::process() process()} and processed 
   * by the {@link RlimgBlockProcessor::replaceCallback() replaceCallback()}.
   * 
   * @return array Array of attributes, keyed by attribute name.
   */
  protected function parseParams($matches) {
    // Run expression, looking for pair like: attribute="value"
    preg_match_all($this->attRegex, $matches[1], $matches, PREG_SET_ORDER);
    
    // Put attributes into associative array
		foreach ($matches as $val) {
      // Let's make the attribute names case insensitive
      $name = strtolower($val[1]);
      
      // Only pass along varibles defined in the class spec
      if (array_key_exists($name, $this->variables)) {
        $atts[$name] = $val[2];
      }
    }
    
    // Get defaults (or other post-processing)
    foreach ($this->variables as $name => $value) {
      $val = isset($atts[$name]) ? $atts[$name] : NULL;
      $atts[$name] = $this->processAttribute($name, $val);
    }
    
    return $atts;
  }
  
  /**
   * Processes a single attribute and returns a cleaned value.
   * 
   * This function will accept a single attribute name and value.
   * If the value is unset or invalid, a default value will be
   * provided.
   * 
   * The available attributes are:
   * - align: the alignment value of the entire block and 
   *   surrounding styling. The available values are 'left',
   *   'center', 'right', and 'none'.
   * - class: a CSS class name whose styling will get added to
   *   the top-level node. Use this to override styling on a single
   *   call.
   * - style: inline CSS styling applied to the top-level of
   *   the themed output. Use this to override styling on a
   *   single call. If you plan to reuse styling, consider
   *   using the 'class' attribute of creating a custom template
   *   override.
   * 
   * @param string $name The attribute name
   * @param string $value The value of the attribute (if any).
   * 
   * @return string Cleaned attribute value.
   */
  protected function processAttribute($file, $name, $value) {
    if (! $name) {
      return NULL;
    }
    
    // Return formatted value
    switch ($name) {
      case 'align':
        if ($value == 'left')
          return 'rlimg-block-align-left';
        if ($value == 'center')
          return 'rlimg-block-align-center';
        if ($value == 'right')
          return 'rlimg-block-align-right';
        return 'rlimg-block-align-none';
      case 'class':
        if (isset($value))
          return $value;
        return 'rlimg-block';
      case 'style':
        if (isset($value))
          return $value;
        return '';
    }
    
    return '';
  }
}